#!/usr/bin/env bash

set -eo pipefail

if [ "$1" != "jira" ]
then
    exec su-exec ${APP_USER} "$@"
fi

# remove dummy jira argument
ARGS="${@:2}"

# reset tomcat connector configuration
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8080']/@proxyName" "${JIRA_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8080']/@proxyPort" "${JIRA_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8080']/@scheme" "${JIRA_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8080']/@secure" "${JIRA_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --update "//Connector[@port='8080']/@redirectPort" --value "8443" "${JIRA_INSTALL}/conf/server.xml"

if [ -n "${X_PROXY_NAME}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert '//Connector[@port="8080"]' --type "attr" --name "proxyName" --value "${X_PROXY_NAME}" "${JIRA_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PROXY_PORT}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert '//Connector[@port="8080"]' --type "attr" --name "proxyPort" --value "${X_PROXY_PORT}" "${JIRA_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PROXY_SCHEME}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert '//Connector[@port="8080"]' --type "attr" --name "scheme" --value "${X_PROXY_SCHEME}" "${JIRA_INSTALL}/conf/server.xml"
fi

if [ "${X_PROXY_SCHEME}" = "https" ]
then
    xmlstarlet edit --inplace --pf --ps --insert '//Connector[@port="8080"]' --type "attr" --name "secure" --value "true" "${JIRA_INSTALL}/conf/server.xml"
    xmlstarlet edit --inplace --pf --ps --update '//Connector[@port="8080"]/@redirectPort' --value "${X_PROXY_PORT}" "${JIRA_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PATH}" ]
then
    xmlstarlet edit --inplace --pf --ps --update '//Context/@path' --value "${X_PATH}" "${JIRA_INSTALL}/conf/server.xml"
fi

exec su-exec ${APP_USER} ${JIRA_INSTALL}/bin/start-jira.sh -fg "$@"
