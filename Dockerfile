FROM blackcowtech/atlassian-base:8.202.08-9.0.13-g50547167
LABEL maintainer="Paul Jefferson <paul.jefferson@blackcow-technology.co.uk"

ARG JIRA_VERSION

ENV \
  JIRA_HOME=${APP_HOME}/jira \
  JIRA_INSTALL=${APP_INSTALL}/jira

EXPOSE 8080

# from https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-${JIRA_VERSION}.tar.gz
COPY atlassian-jira-${JIRA_VERSION}.tar.gz /tmp/

RUN set -ex \
  && apk add --update --no-cache --virtual .build-deps \
  shadow \
  tar \
  && mkdir -p ${JIRA_HOME} \
  && mkdir -p ${JIRA_INSTALL} \
  && usermod -d ${JIRA_HOME} ${APP_USER} \
  && tar xzf /tmp/atlassian-jira-${JIRA_VERSION}.tar.gz -C ${JIRA_INSTALL} --strip-components=1 \
  && chown -R ${APP_USER}:${APP_GROUP} ${JIRA_HOME} \
  && chown -R ${APP_USER}:${APP_GROUP} ${JIRA_INSTALL} \
  # tidy up
  && apk del .build-deps \
  && rm -rf /tmp/* \
  && rm -rf /var/log/*

WORKDIR ${JIRA_HOME}
VOLUME ["${JIRA_HOME}"]

COPY entrypoint.sh /
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/sbin/tini","--","/entrypoint.sh"]
CMD ["jira"]
