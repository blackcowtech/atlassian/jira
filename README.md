# Atlassian JIRA Docker Image

## Deprecation
This image has been deprecated in favour of the [atlassian/jira-software](https://hub.docker.com/r/atlassian/jira-software), and due to deprecation of an image on which this image is based [frolvlad/alpine-oraclejdk8](https://github.com/Docker-Hub-frolvlad/docker-alpine-java).

No futher updates will be made to this image, and the base image will be fixed at [blackcowtech/atlassian-base:8.202.08-9.0.13-g50547167](https://hub.docker.com/layers/blackcowtech/atlassian-base/8.202.08-9.0.13-g50547167/images/sha256-2921cff4fb86c9c2114bf18209f41b64ad30bc45d554cddce3c5df5ff815ffa9).

| Application | Current Version |
|-------------|-----------------|
| JIRA Software | 8.4.1 |

The key features of the image are:

* Alpine Linux Based
* Oracle Java
* Apache Tomcat with Apache Portable Runtime (APR) support

The image is built on [blackcowtech/atlassian-base](https://gitlab.com/blackcowtech/atlassian/base), which in turn is built on [frolvlad/alpine-oraclejdk8](https://github.com/frol/docker-alpine-oraclejdk8) ([MIT License](https://github.com/frol/docker-alpine-oraclejdk8/blob/master/LICENSE)) with a chunk of [docker-library/tomcat](https://github.com/docker-library/tomcat) ([Apache License 2.0](https://github.com/docker-library/tomcat/blob/master/LICENSE)).

As highlighted in the frolvlad/alpine-oraclejdk8 [readme](https://github.com/frol/docker-alpine-oraclejdk8/blob/master/README.md):

> You must accept the [Oracle Binary Code License Agreement for Java SE](http://www.oracle.com/technetwork/java/javase/terms/license/index.html) to use this image (see #6 for details).

Other Dockerized Atlassian applications are also available:

* [blackcowtech/atlassian-bamboo](https://gitlab.com/blackcowtech/atlassian/bamboo)
* [blackcowtech/atlassian-bitbucket](https://gitlab.com/blackcowtech/atlassian/bitbucket)
* [blackcowtech/atlassian-confluence](https://gitlab.com/blackcowtech/atlassian/confluence)

Much of the inspiration for these Dockerfiles has come from [blacklabelops](https://github.com/blacklabelops) and [cptactionhank](https://github.com/cptactionhank).
